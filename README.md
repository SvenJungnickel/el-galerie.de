![EL-Galerie Logo](http://www.el-galerie.de/wp-content/themes/el-galerie.de/images/logo.png "EL-Galerie Logo")

# Online shop www.EL-Galerie.de

A Client wished for a online store to sell his own images. I developed the store based on [WordPress](https://wordpress.com) and a shop plugin named [wpShopGermany](http://wpshopgermany.maennchen1.de). 

I programmed a new WordPress theme from the scratch using the [bootstrap](http://getbootstrap.com) frontend framework and integrated the jQuery plugin [Fancybox](http://fancybox.net).

I also created a Wordpress widget plugin and integrated the [iosslider](https://iosscripts.com/iosslider/) script.