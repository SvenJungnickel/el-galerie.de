<?php
/**
 * el-galerie.de
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

//namespace ElgalerieCategoryWidget\controller;


/**
 * Elgalerie_Category_Widget
 *
 * Widget main class
 *
 * @author    Sven Jungnickel <svenjungnickel@googlemail.com>
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
class Elgalerie_Category_Widget extends \WP_Widget {

    /**
     * Register widget. Set Name and Description
     */
    public function __construct()
    {
        parent::__construct(
            'elgalerie_category_widget',
            __( 'EL-Galerie.de Category Widget', 'el-galerie.de' ),
            array(
                'description' => __( 'Custom widget area for the startpage of el-galerie.de', 'el-galerie.de' )
            )
        );
    }

    /**
     * Display admin form
     *
     * @param array $instance
     * @return string
     */
    public function form($instance)
    {
        $defaults = array(
            'limit' => '3',
            'orderBy' => 'date',
            'order' => 'DESC'
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php echo  __("Kategorie", "elgalerie"); ?></label>
            <?php wp_dropdown_categories( array(
                'hide_empty' => 0,
                'show_count' => 1,
                'class'      => 'widefat',
                'id'         => $this->get_field_name('category'),
                'name'       => $this->get_field_name('category'),
                'selected'   => $instance['category']
            ) ); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo  __("Titel", "elgalerie"); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php echo __("Anzahl", "elgalerie"); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo esc_attr($instance['limit']); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('orderBy'); ?>"><?php echo __("Sortierung nach", "elgalerie"); ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('orderBy'); ?>" name="<?php echo $this->get_field_name('orderBy'); ?>">
                <option value="date"<?php selected( $instance['orderBy'], 'date' ); ?>><?php _e('Datum','elgalerie')?></option>
                <option value="title"<?php selected( $instance['orderBy'], 'title' ); ?>><?php _e('Titel','elgalerie')?></option>
                <option value="rand"<?php selected( $instance['orderBy'], 'rand' ); ?>><?php _e('Zufällig','elgalerie')?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('order'); ?>"><?php echo __("Sortierreihenfolge", "elgalerie"); ?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
                <option value="desc"<?php selected( $instance['order'], 'desc' ); ?>><?php _e('Absteigend','elgalerie')?></option>
                <option value="asc"<?php selected( $instance['order'], 'asc' ); ?>><?php _e('Aufsteigend','elgalerie')?></option>
            </select>
        </p>
        <?php
    }

    /**
     * Save widget settings
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['category'] = (int)($new_instance['category']);
        $instance['limit'] = (int)$new_instance['limit'];
        $instance['orderBy'] = $new_instance['orderBy'];
        $instance['order'] = $new_instance['order'];

        return $instance;
    }

    /**
     * Display frontend content
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $category = apply_filters('category', $instance['category']);

        $args = array(
            'cat'       => $category,
            'showposts' => $instance['limit'],
            'orderby'   => $instance['orderBy'],
            'order'     => $instance['order']
        );

        $posts = new WP_Query( $args );
        if($posts->have_posts()) {

            // Frontend-Template einbinden
            require __DIR__ . '/../views/category.phtml';

            wp_reset_postdata();
        }
    }
}