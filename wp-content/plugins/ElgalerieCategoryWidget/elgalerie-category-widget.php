<?php
/*
  Plugin Name: EL-Galerie.de Category Widget
  Plugin URI: http://www.el-galerie.de
  Description: Custom widget area for the category selection of el-galerie.de
  Version: 0.1
  Author: Sven Jungnickel
  Author URI: http://www.el-galerie.de
 */

//namespace ElgaleryCategoryWidget;
//
//require_once __DIR__ . '/Autoloader.php';
//
//// register AutoLoader
//$autoLoader = new AutoLoader(__DIR__ . '/', 'ElgaleryCategoryWidget');
//$autoLoader->register();

require_once __DIR__ . '/controller/Elgalerie_Category_Widget.php';

// Register widget
add_action('widgets_init', function () {
    register_widget('Elgalerie_Category_Widget');
});

// Register css and javascript
add_action( 'wp_enqueue_scripts', 'Elgalerie_Category_Widget_CSS' );
function Elgalerie_Category_Widget_CSS() {
    wp_enqueue_style( 'iosslider', plugins_url('js/iosslider/iosslider.css', __FILE__) );
    wp_enqueue_script( 'iosslider', plugins_url('js/iosslider/jquery.iosslider.min.js', __FILE__), array(), false, true );
    wp_enqueue_script( 'easing', plugins_url('js/jquery.easing-1.3.js', __FILE__), array(), false, true );
    wp_enqueue_script( 'script', plugins_url('js/initial.js', __FILE__), array(), false, true );
}