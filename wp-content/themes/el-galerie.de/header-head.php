<?php
/**
 * el-galerie.de
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php //TODO: META Angaben dynamisch ermitteln (evtl über eigene Theme Option?) ?>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php bloginfo('template_url');?>/favicon.ico" type="image/x-icon">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url');?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php bloginfo('template_url');?>/bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <?php //TODO: DEBUG Modus dynamisch ermitteln (evtl über eigene Theme Option?) ?>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?php bloginfo('template_url');?>/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php bloginfo('template_url');?>/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/bootstrap/js/jquery.min.js"><\/script>')</script>

    <?php wp_head(); ?>

    <!-- own styles -->
    <link href="<?php bloginfo('template_url');?>/elgalerie.css" rel="stylesheet">
</head>
<body>
    <div class="content">
        <!-- TOP NAV
        ================================================== -->
        <div class="container hidden-xs">
            <div class="top-nav-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            &nbsp;
                        </div>
                        <?php //TODO: Hier sollte eventuell das Kundenverwaltungs- und/oder Warenkorb-Widget von wpShopGermany eingebunden werden ?>
                        <?php foreach( wp_get_nav_menu_items('Top-Nav') as $topnav) { ?>
                            <div class="top-nav-start-item col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <p class="top-nav-start-item-inner">
                                    <a href="<?=$topnav->url?>">
                                        <?=$topnav->title?>
                                    </a>
                                </p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.top nav -->
