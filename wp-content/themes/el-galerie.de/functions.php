<?php
/**
 * el-galerie
 *
 * Themespezifische Optionen
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

/** Register Custom Navigation Walker */
require_once('wp_bootstrap_navwalker.php');

/** Menüs im Theme aktivieren */
register_nav_menu('primary', 'Menü');
register_nav_menu('secondary', 'Top-Nav');
register_nav_menu('third', 'Footer');

/** Sidebars im Theme aktivieren */
register_sidebar(
    array(
        'name' => 'Startseite',
        'id'   => 'startpage'
    )
);

/** automatisch ein Suchfeld zum wp-nav-menu hinzufügen. */
add_filter('wp_nav_menu_items','add_search_box', 10, 2);
function add_search_box($items, $args)
{
    if( $args->theme_location == 'primary') {
        $items .= '<li class="menu-item menu-item-search pull-right hidden-xs">' . get_search_form(false) . '</li>';
    }

    return $items;
}
