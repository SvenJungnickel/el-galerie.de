<?php
/**
 * el-galerie.de
 *
 * Template wenn keine Suchergebnisse gefunden wurden
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <p>
        <?php
            if(get_search_query()) {
                printf(
                    __('Keine Ergebnisse gefunden für: %s', 'elgalerie'),
                    '<strong>' . get_search_query() . '</strong>'
                );
            } else {
                echo __('Bitte einen Suchbegriff eingeben', 'elgalerie');
            }
        ?>
    </p>
</div>
