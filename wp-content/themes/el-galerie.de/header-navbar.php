<?php
/**
 * el-galerie.de
 *
 * Hauptmenü
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>

<!-- NAVBAR
================================================== -->
<div class="container">
    <nav class="navbar">
        <div class="navbar-header">
            <div class="row">
                <div class="visible-xs-block pull-left col-xs-10">
                    <?php
                    /**
                     * Suchformular für mobile Geräte anzeigen
                     */
                    get_template_part('searchform');
                    ?>
                </div>
                <div class="col-xs-2">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php
            /**
             * Menü mit Bootstrap Walker ausgeben
             * Navwalker auf https://github.com/twittem/wp-bootstrap-navwalker herunterladen
             * */
            $mainMenu = array(
                'theme_location' => 'primary',
                'menu'           => 'Menü',
                'container'      => false,
                'menu_class'     => 'nav navbar-nav',
                'depth'          => 2,
                'walker'         => new wp_bootstrap_navwalker()
            );
            wp_nav_menu($mainMenu);

            /**
             * Top-Menü nur auf mobilen Geräten anzeigen
             */
            $topNav = array(
                'theme_location' => 'secondary',
                'menu'           => 'Top-Nav',
                'container'      => false,
                'menu_class'     => 'visible-xs-block nav navbar-nav',
                'depth'          => 2,
                'walker'         => new wp_bootstrap_navwalker()
            );
            wp_nav_menu($topNav);
            ?>
        </div>
    </nav>
</div>
<!-- /.navbar -->
