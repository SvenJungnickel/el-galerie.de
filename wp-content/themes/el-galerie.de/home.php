<?php
/**
 * el-galerie
 *
 * Startseite
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header('head');
?>

<!-- LOGO
================================================== -->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <div class="logo-start-wrapper">
                <div class="logo-subtitle">
                    <span>Der Bilder-Shop</span>
                </div>
                <div class="clearfix"></div>
                <div class="logo-inner">
                    <div class="logo-img">
                        <a href="<?php bloginfo('url');?>">
                            <?php //TODO: Logo dynamisch nachladen per Theme Option ?>
                            <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/logo.png">
                        </a>
                    </div>
                </div>
                <div class="logo-subtitle">
                    <span><?php bloginfo('description');?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Logo -->



<!-- Carousel
================================================== -->
<?php
$i=0;
$posts = get_posts('category_name=Startseiten-Slider');
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        foreach ( $posts as $post ) : setup_postdata( $post );
            if ( has_post_thumbnail() ) {
                ?>
                <li data-target="#myCarousel" data-slide-to="<?= $i; ?>"<?php if ($i == 0) echo ' class="active"'; ?>></li>
                <?php
                $i++;
            }
        endforeach;
        $i=0;
        ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php
        foreach ( $posts as $post ) : setup_postdata( $post );
            if ( has_post_thumbnail() ) {
                // TODO: evtl 'large' (max 1600px) statt 'full' Image aus Performancengründen verwenden?
                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
                ?>
                <div class="item<?php if($i==0) echo ' active';?>" style="background-image: url('<?=$thumbnail[0];?>')">
                </div>
                <?php
                $i++;
            }
        endforeach;
        wp_reset_postdata();
        ?>
    </div>
    <a class="left carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control hidden-xs" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>


    <!-- Header Text
    ================================================== -->
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-7 col-sm-5 col-md-offset-7 col-md-5 col-lg-offset-8 col-lg-4">
                <div class="head-text-start">
                    <div class="head-text-start-inner">
                        <?= get_post_field('post_content', get_page_by_path('headertext')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.Header Text -->


</div>
<!-- /.carousel -->



<!-- Navbar einbinden -->
<?php get_header('navbar'); ?>



<!-- Start page text
================================================== -->
<div class="container">

    <!-- Oster-Banner -->
    <?php if(strftime('%Y%m%d') < 20160401) { ?>
        <a href="<?php bloginfo('url');?>/alle-motive/">
            <img src="<?php bloginfo('template_url'); ?>/images/oster-sale-banner.png" class="img-responsive">
        </a>
        <p>&nbsp;</p>
    <?php } ?>

    <div class="start-text">
        <?= get_post_field('post_content', get_page_by_path('startseitentext')); ?>
    </div>
</div>
<!-- /. Start page text -->



<!-- Widget for start page categories -->
<?php if ( is_active_sidebar( 'startpage' ) ) :
    dynamic_sidebar( 'startpage' );
endif; ?>
<!-- /. Widget start page categories -->



<?php get_footer(); ?>