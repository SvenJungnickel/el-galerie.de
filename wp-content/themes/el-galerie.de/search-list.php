<?php
/**
 * el-galerie.de
 *
 * Suchergebniss Liste
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>
<div class="container category-container">
    <?php if (have_posts() && get_search_query()) : ?>
        <div class="row">
            <div class="category-title">
                <h1>
                    <?php printf(__('Suche nach: %s', 'elgalerie'), '<strong>' . get_search_query() . '</strong>'); ?>
                    <?php printf(__('(%s Ergebnisse)', 'elgalerie'), $wp_query->found_posts); ?>
                </h1>
            </div>
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile; ?>
        </div>
    <?php else :
        get_template_part('search', 'none');
    endif ?>
</div>