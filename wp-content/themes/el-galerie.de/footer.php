<?php
/**
 * el-galerie
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>
</div>
    <!-- FOOTER -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p><?php bloginfo('name');?></p>
                    <p>&nbsp;</p>
                    <?php //TODO: Impressumsangaben dynamisch nachladen per Theme Option ?>
                    <p>Manuela Reints-von Byern</p>
                    <p>Nagelshof 15 a</p>
                    <p>49716 Meppen</p>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <?php //TODO: Impressumsangaben dynamisch nachladen per Theme Option ?>
                    <p>Kontakte</p>
                    <p>&nbsp;</p>
                    <p>Tel: 0 59 31 / 4 96 23 97</p>
                    <p>Mobil: 01 76 / 27 67 96 29</p>
                    <p>Mail: <a href="<?= get_permalink(12); ?>">Kontakt@EL-Galerie.de</a></p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <p><?= get_post_field('post_content', get_page_by_path('footertext')); ?></p>
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-md-3 col-lg-3">
                        &nbsp;
                    </div>
                    <?php foreach( wp_get_nav_menu_items('Footer') as $footer) { ?>
                    <div class="footer-bottom-item col-sm-3 col-md-2 col-lg-2">
                        <p class="footer-bottom-item-inner">
                            <a href="<?=$footer->url?>">
                                <?=$footer->title?>
                            </a>
                        </p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php bloginfo('template_url');?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!--    <script src="--><?php //bloginfo('template_url');?><!--/bootstrap/js/holder.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php bloginfo('template_url');?>/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    <?php wp_footer(); ?>
</body>
</html>
