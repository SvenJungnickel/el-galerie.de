<?php
/**
 * el-galerie
 *
 * Anzeige des einzelnen Beitrags (Produkts)
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header();

the_post();

// Einbinden des Produkt-Templates per Content
the_content();

get_footer();
