<?php
/**
 * el-galerie.de
 *
 * Template für das Suchformular
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>
<div class="search-form-wrapper">
    <form method="get" class="search-form" id="searchform" action="<?php bloginfo('url'); ?>/">
        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="<?php echo __('Suche...', 'elgalerie'); ?>" />
        <button type="submit" class="hidden-xs">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
        </button>
    </form>
</div>