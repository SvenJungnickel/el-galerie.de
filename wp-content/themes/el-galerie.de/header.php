<?php
/**
 * el-galerie
 *
 * Header
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header('head');
?>

<!-- LOGO
================================================== -->
<?php
//TODO: Header-Hintergrund dynamisch (Aus Theme Optionen?) ermitteln
//$slider = get_posts('category_name=Startseiten-Slider');
//$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($slider[0])->ID, 'full')[0];
$thumbnail = 'http://el-galerie.de/wp-content/uploads/header-bg.jpg';
?>
<div class="logo-background" style="background-image: url('<?=$thumbnail;?>')">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <div class="logo-wrapper">
                    <div class="logo-inner">
                        <div class="logo-img">
                            <a href="<?php bloginfo('url');?>">
                                <?php //TODO: Logo dynamisch nachladen per Theme Option ?>
                                <img class="img-responsive" src="<?php bloginfo('template_url');?>/images/logo.png">
                            </a>
                        </div>
                    </div>
                    <div class="logo-subtitle">
                        <span><?php bloginfo('description');?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Logo -->


<!-- Navbar einbinden -->
<?php get_header('navbar'); ?>