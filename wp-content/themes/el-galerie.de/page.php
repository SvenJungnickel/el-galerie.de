<?php
/**
 * el-galerie.de
 *
 * Anzeige der statischen Seiten
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header();
?>

<?php the_post(); ?>
<div class="container page-content">
    <div class="row">
        <div class="page-title">
            <h1><?php echo the_title(); ?></h1>
        </div>
        <?php the_content();?>
    </div>
</div>

<?php get_footer(); ?>
