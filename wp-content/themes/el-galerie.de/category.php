<?php
/**
 * el-galerie
 *
 * Anzeige der Produktkategorie
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header();

get_template_part('category', 'list');

get_footer();
