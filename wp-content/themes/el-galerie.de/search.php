<?php
/**
 * el-galerie.de
 *
 * Template für Suchergbnisse
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */

get_header();

get_template_part('search', 'list');

get_footer();