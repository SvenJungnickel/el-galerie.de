<?php
/**
 * el-galerie.de
 *
 * Kategorie Liste
 *
 * @copyright Copyright (c) 2015 Sven Jungnickel
 */
?>
<div class="container category-container">
    <?php if (have_posts()) : ?>
        <div class="row">
            <div class="category-title">
                <h1><?php echo single_cat_title(); ?></h1>
            </div>
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
    <?php endif ?>
</div>